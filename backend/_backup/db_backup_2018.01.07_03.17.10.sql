-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `admin`
-- -------------------------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `ID_ADMIN` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `NAMA_ADMIN` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(40) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `STATUS` int(5) DEFAULT NULL,
  `JOB_DESC` varchar(20) DEFAULT NULL,
  `pic` varchar(255) NOT NULL DEFAULT '/img/staff.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`ID_ADMIN`),
  KEY `ID_USER` (`user_id`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `daftar_periksa`
-- -------------------------------------------
DROP TABLE IF EXISTS `daftar_periksa`;
CREATE TABLE IF NOT EXISTS `daftar_periksa` (
  `ID_DAFTAR` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PASIEN` int(11) DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `TANGGAL_PERIKAS` date DEFAULT NULL,
  `KELUHAN` text,
  `STATUS` varchar(5) DEFAULT NULL,
  `WAKTU_DAFTAR` time DEFAULT NULL,
  `ID_URUT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_DAFTAR`),
  KEY `ID_PASIEN` (`ID_PASIEN`),
  KEY `ID_DOKTER` (`ID_DOKTER`),
  KEY `ID_URUT` (`ID_URUT`),
  CONSTRAINT `daftar_periksa_ibfk_1` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`),
  CONSTRAINT `daftar_periksa_ibfk_2` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  CONSTRAINT `daftar_periksa_ibfk_3` FOREIGN KEY (`ID_URUT`) REFERENCES `urut_periksa` (`ID_URUT`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `dokter`
-- -------------------------------------------
DROP TABLE IF EXISTS `dokter`;
CREATE TABLE IF NOT EXISTS `dokter` (
  `ID_DOKTER` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) DEFAULT NULL,
  `NAMA_DOKTER` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `NO_PRAKTEK` varchar(20) DEFAULT NULL,
  `SPESIALIS` varchar(30) DEFAULT NULL,
  `STATUS` int(5) DEFAULT '0',
  `STATUS_AKUN` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`ID_DOKTER`),
  KEY `ID_USER` (`ID_USER`),
  CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `jadwal_dokter`
-- -------------------------------------------
DROP TABLE IF EXISTS `jadwal_dokter`;
CREATE TABLE IF NOT EXISTS `jadwal_dokter` (
  `ID_JADWAL` int(11) NOT NULL AUTO_INCREMENT,
  `WAKTU_MULAI` time DEFAULT NULL,
  `WAKTU_SELESAI` time DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_JADWAL`),
  KEY `ID_DOKTER` (`ID_DOKTER`),
  CONSTRAINT `jadwal_dokter_ibfk_1` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `migration`
-- -------------------------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `obat`
-- -------------------------------------------
DROP TABLE IF EXISTS `obat`;
CREATE TABLE IF NOT EXISTS `obat` (
  `ID_OBAT` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA_OBAT` varchar(40) DEFAULT NULL,
  `HARGA` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_OBAT`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `pasien`
-- -------------------------------------------
DROP TABLE IF EXISTS `pasien`;
CREATE TABLE IF NOT EXISTS `pasien` (
  `ID_PASIEN` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `NAMA_PASIEN` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(40) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `BERAT_BADAN` int(11) DEFAULT NULL,
  `TINGGI_BADAN` int(11) DEFAULT NULL,
  `GOL_DARAH` varchar(2) DEFAULT NULL,
  `STATUS` int(5) DEFAULT '10',
  PRIMARY KEY (`ID_PASIEN`),
  KEY `ID_USER` (`ID_USER`),
  CONSTRAINT `pasien_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `pembayaran`
-- -------------------------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE IF NOT EXISTS `pembayaran` (
  `ID_PEMBAYARAN` int(11) NOT NULL AUTO_INCREMENT,
  `BAYAR_PERIKSA` varchar(20) DEFAULT NULL,
  `TOTAL_BAYAR` varchar(20) DEFAULT NULL,
  `ID_RESEP` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_PEMBAYARAN`),
  KEY `ID_RESEP` (`ID_RESEP`),
  CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`ID_RESEP`) REFERENCES `resep` (`ID_RESEP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `periksa`
-- -------------------------------------------
DROP TABLE IF EXISTS `periksa`;
CREATE TABLE IF NOT EXISTS `periksa` (
  `ID_PERIKSA` int(11) NOT NULL AUTO_INCREMENT,
  `DIAGNOSA` text,
  `CATATAN` text,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `ID_PASIEN` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_PERIKSA`),
  KEY `ID_DOKTER` (`ID_DOKTER`),
  KEY `ID_PASIEN` (`ID_PASIEN`),
  CONSTRAINT `periksa_ibfk_1` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  CONSTRAINT `periksa_ibfk_2` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `resep`
-- -------------------------------------------
DROP TABLE IF EXISTS `resep`;
CREATE TABLE IF NOT EXISTS `resep` (
  `ID_RESEP` int(11) NOT NULL AUTO_INCREMENT,
  `TOTAL_HARGA` varchar(20) DEFAULT NULL,
  `ID_OBAT` int(11) DEFAULT NULL,
  `ID_PERIKSA` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_RESEP`),
  KEY `ID_OBAT` (`ID_OBAT`),
  KEY `ID_PERIKSA` (`ID_PERIKSA`),
  CONSTRAINT `resep_ibfk_1` FOREIGN KEY (`ID_OBAT`) REFERENCES `obat` (`ID_OBAT`),
  CONSTRAINT `resep_ibfk_2` FOREIGN KEY (`ID_PERIKSA`) REFERENCES `periksa` (`ID_PERIKSA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `staff`
-- -------------------------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `ID_STAFF` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `NAMA_STAFF` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `JOB_DESC` varchar(20) NOT NULL,
  `STATUS` int(5) DEFAULT '10',
  `pic` varchar(255) NOT NULL DEFAULT '/img/staff.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`ID_STAFF`),
  KEY `ID_USER` (`user_id`),
  CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `urut_periksa`
-- -------------------------------------------
DROP TABLE IF EXISTS `urut_periksa`;
CREATE TABLE IF NOT EXISTS `urut_periksa` (
  `ID_URUT` int(11) NOT NULL AUTO_INCREMENT,
  `NO_URUT` int(11) DEFAULT NULL,
  `WAKTU_PERIKSA` time DEFAULT NULL,
  PRIMARY KEY (`ID_URUT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `user`
-- -------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -------------------------------------------
-- TABLE DATA daftar_periksa
-- -------------------------------------------
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('1','1','1','2017-12-04','Batuk Berdahak','1','03:43:00','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('2','2','1','2017-12-04','Demam tinggi, batuk, pilek','1','05:43:00','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('5','1','1','2017-12-03','Gatal Gatal','1','22:35:00','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('7','','','','','','','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('8','','','','','','','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('9','','','','','','','');



-- -------------------------------------------
-- TABLE DATA dokter
-- -------------------------------------------
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('1','1','Wisnu Maulana','Koncopait@gmail.com','wisnumaulanat','123456','08122971126','Gulunan, Brujul, Jaten, Karanganyar','Islam','Karanganyar','1997-07-16','12480124','Mata','0','10');
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('2','1','Syahrul Munir','munir@gmail.com','muneron','123','08122971126','Gulunanl Brujul, Jaten, Karanganyar','Islam','Karanganyar','1997-07-16','12124224','Mata','0','10');
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('3','1','','','andihi','123456','','','','','','','','0','0');
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('4','','Abdul Latief','abdullatief@gmail.com','abdullatief','123456','','','','','','','','0','0');
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('5','','Lely','lely@gmail.com','lely','123456','','','','','','','','0','10');



-- -------------------------------------------
-- TABLE DATA jadwal_dokter
-- -------------------------------------------
INSERT INTO `jadwal_dokter` (`ID_JADWAL`,`WAKTU_MULAI`,`WAKTU_SELESAI`,`ID_DOKTER`) VALUES
('1','01:00:00','11:30:00','1');
INSERT INTO `jadwal_dokter` (`ID_JADWAL`,`WAKTU_MULAI`,`WAKTU_SELESAI`,`ID_DOKTER`) VALUES
('2','01:00:00','12:00:00','2');



-- -------------------------------------------
-- TABLE DATA migration
-- -------------------------------------------
INSERT INTO `migration` (`version`,`apply_time`) VALUES
('m000000_000000_base','1511447175');
INSERT INTO `migration` (`version`,`apply_time`) VALUES
('m130524_201442_init','1511447178');



-- -------------------------------------------
-- TABLE DATA obat
-- -------------------------------------------
INSERT INTO `obat` (`ID_OBAT`,`NAMA_OBAT`,`HARGA`) VALUES
('1','Paracetamol','10000');
INSERT INTO `obat` (`ID_OBAT`,`NAMA_OBAT`,`HARGA`) VALUES
('2','CTM','3000');



-- -------------------------------------------
-- TABLE DATA pasien
-- -------------------------------------------
INSERT INTO `pasien` (`ID_PASIEN`,`ID_USER`,`NAMA_PASIEN`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`BERAT_BADAN`,`TINGGI_BADAN`,`GOL_DARAH`,`STATUS`) VALUES
('1','1','Wisnu Maulana','koncopait1@gmail.com','Pasien1','pasien','087835121007','sroyo, jaten, karanganyar','islam','karanganyar','2018-01-10','75','170','O','1');
INSERT INTO `pasien` (`ID_PASIEN`,`ID_USER`,`NAMA_PASIEN`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`BERAT_BADAN`,`TINGGI_BADAN`,`GOL_DARAH`,`STATUS`) VALUES
('2','1','Syahrul munir','koncopait11@gmail.com','Pasien11','pasien','','','','','0000-00-00','0','0','','1');



-- -------------------------------------------
-- TABLE DATA periksa
-- -------------------------------------------
INSERT INTO `periksa` (`ID_PERIKSA`,`DIAGNOSA`,`CATATAN`,`ID_DOKTER`,`ID_PASIEN`) VALUES
('1','Diare','Jangan Makan Pedas','1','2');
INSERT INTO `periksa` (`ID_PERIKSA`,`DIAGNOSA`,`CATATAN`,`ID_DOKTER`,`ID_PASIEN`) VALUES
('3','Pilek','Jangan Begadang','1','2');



-- -------------------------------------------
-- TABLE DATA staff
-- -------------------------------------------
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('14','14','Asep Nugraha','','admin','admin','087835121008','','Islam','','','Admin Klinik','','upload/profile/14.jpg','2017-12-05 03:19:52','2018-01-07 03:03:07');
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('15','38','','','','','','','','','','','','/img/staff.jpg','2018-01-06 01:25:17','2018-01-06 01:25:17');
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('16','39','','','','','','','','','','','','/img/staff.jpg','2018-01-06 02:10:50','2018-01-06 02:10:50');
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('17','40','','','','','','','','','','','10','/img/staff.jpg','2018-01-07 02:10:02','2018-01-07 02:10:02');
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('18','41','','','','','','','','','','','10','/img/staff.jpg','2018-01-07 02:11:06','2018-01-07 02:11:06');
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('19','42','','','','','','','','','','','10','/img/staff.jpg','2018-01-07 02:14:18','2018-01-07 02:14:18');



-- -------------------------------------------
-- TABLE DATA user
-- -------------------------------------------
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('1','WIsnu','42YxZEEV_rP6FFxQ9ViLScpds4H2mdnZ','$2y$13$oSCNwzYVLyOsDHhLunrpN.RsOpP3KirGiV4F5R1dS0gX0SpaV5wOS','','koncopait@gmail.com','10','0','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('4','admin','zDmrClhhAIisc1ggkIKhgqk2xkZsrWg-','$2y$13$BpOtGXsoaBfZ2Ag4D7/xZOLT9wOG3/YNlKeluC.LaEXXHGLyjLEWe','','admin@gmail.com','10','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('5','alasepnug','_88Vbx3xKgq53oQiZjpXAs0rZbMtFCpQ','$2y$13$Ux5xWmoKfNEACaQxfYLwnu6HrwMA9WHgwym5LLERLGxbOpmeZqFdq','','al.asepnug@gmail.com','10','0','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('14','andihi','LPaSsvLdNRpGRBigXg6UUSCaKuz_VyBX','$2y$13$kHgdhAHJxTP11W69Y8HR9O3dH757UnO09O3zkqh.ymkMuda265nuC','','andihi@gmail.com','10','20','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('29','nugroho','gwu-xCT1QzY0sWjwB-5hzvU-t3sZeT2b','$2y$13$ib6hcMcgS4Y.LuRhqphpHeMo2Cqr81j9koUh.O3EhAVGNI8cNemt6','','nugroho@gmail.com','10','10','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('30','abdullatief','EcZ0lNw992AItFtFNzX_33hl_4pYKys4','$2y$13$bpr7zpECMVz0DvTWbuRJJeW6gbB6sFRatyAVH7HGCmJz7dN7zcMb2','','abdullatief@gmail.com','10','10','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('38','lely','CeDEH2u1PLWiX7jTfk0PSA06DkL-MWrf','$2y$13$32BE4beGsRghbJ6lb8D67uF1sCbcqF5IolsV/0slKC4Mbi3QVvg2m','','lely@gmail.com','10','10','2018-01-06 01:25:16','2018-01-06 01:25:16');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('39','dul','XaEMfwOM1WQbVZZQGUqrrLBvbabkesZC','$2y$13$d04VB41ikLlFUMz7i4UkG./fltIpcAJ2ukywVJRTFuGm/9A1K6G7q','','dul@gmail.com','10','10','2018-01-06 02:10:50','2018-01-06 02:10:50');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('40','budi','qwGg7le-9KnfM_LJ9jtxP7RcHIGE4yez','$2y$13$t3iwPGy/iKNfKinN1PCdYuSoZpu4UbqsGWF.sFbCZuqLCwIP1Qq6K','','budi@gmail.com','10','10','2018-01-07 02:10:02','2018-01-07 02:10:02');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('41','dani','ANag4v6Z_STWXsN8srRpjkwKK4TdGAsa','$2y$13$6NgRmDmX5hyXb9QyDcWWP.OPKYW9PPos3e4SQ0bzWVl/VoxX.ZtX2','','dani@gmail.com','10','20','2018-01-07 02:11:06','2018-01-07 02:11:06');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('42','mimin','Ub7sxcDT8t65LpANuNHV7bmkcXft3E6L','$2y$13$TVHh1m4jW87jjzkUtWMRFebhf.OLfrb8GIGkDhZraSZn3BP5Bx3JS','','mimin@gmail.com','10','20','2018-01-07 02:14:18','2018-01-07 02:14:18');



-- -------------------------------------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;
-- -------------------------------------------
-- -------------------------------------------
-- END BACKUP
-- -------------------------------------------
