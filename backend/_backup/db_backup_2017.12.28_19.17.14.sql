-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `admin`
-- -------------------------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `ID_ADMIN` int(10) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `NAMA_ADMIN` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(40) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `STATUS` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID_ADMIN`),
  KEY `ID_USER` (`ID_USER`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `daftar_periksa`
-- -------------------------------------------
DROP TABLE IF EXISTS `daftar_periksa`;
CREATE TABLE IF NOT EXISTS `daftar_periksa` (
  `ID_DAFTAR` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PASIEN` int(11) DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `TANGGAL_PERIKAS` date DEFAULT NULL,
  `KELUHAN` text,
  `STATUS` varchar(5) DEFAULT NULL,
  `WAKTU_DAFTAR` time DEFAULT NULL,
  `ID_URUT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_DAFTAR`),
  KEY `ID_PASIEN` (`ID_PASIEN`),
  KEY `ID_DOKTER` (`ID_DOKTER`),
  KEY `ID_URUT` (`ID_URUT`),
  CONSTRAINT `daftar_periksa_ibfk_1` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`),
  CONSTRAINT `daftar_periksa_ibfk_2` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  CONSTRAINT `daftar_periksa_ibfk_3` FOREIGN KEY (`ID_URUT`) REFERENCES `urut_periksa` (`ID_URUT`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `dokter`
-- -------------------------------------------
DROP TABLE IF EXISTS `dokter`;
CREATE TABLE IF NOT EXISTS `dokter` (
  `ID_DOKTER` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `NAMA_DOKTER` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `NO_PRAKTEK` varchar(20) DEFAULT NULL,
  `SPESIALIS` varchar(30) DEFAULT NULL,
  `STATUS` int(5) DEFAULT NULL,
  `STATUS_AKUN` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`ID_DOKTER`),
  KEY `ID_USER` (`ID_USER`),
  CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `jadwal_dokter`
-- -------------------------------------------
DROP TABLE IF EXISTS `jadwal_dokter`;
CREATE TABLE IF NOT EXISTS `jadwal_dokter` (
  `ID_JADWAL` int(11) NOT NULL AUTO_INCREMENT,
  `WAKTU_MULAI` time DEFAULT NULL,
  `WAKTU_SELESAI` time DEFAULT NULL,
  `ID_DOKTER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_JADWAL`),
  KEY `ID_DOKTER` (`ID_DOKTER`),
  CONSTRAINT `jadwal_dokter_ibfk_1` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `migration`
-- -------------------------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `obat`
-- -------------------------------------------
DROP TABLE IF EXISTS `obat`;
CREATE TABLE IF NOT EXISTS `obat` (
  `ID_OBAT` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA_OBAT` varchar(40) DEFAULT NULL,
  `HARGA` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_OBAT`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `pasien`
-- -------------------------------------------
DROP TABLE IF EXISTS `pasien`;
CREATE TABLE IF NOT EXISTS `pasien` (
  `ID_PASIEN` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `NAMA_PASIEN` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(40) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `BERAT_BADAN` int(11) DEFAULT NULL,
  `TINGGI_BADAN` int(11) DEFAULT NULL,
  `GOL_DARAH` varchar(2) DEFAULT NULL,
  `STATUS` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID_PASIEN`),
  KEY `ID_USER` (`ID_USER`),
  CONSTRAINT `pasien_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `pembayaran`
-- -------------------------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE IF NOT EXISTS `pembayaran` (
  `ID_PEMBAYARAN` int(11) NOT NULL AUTO_INCREMENT,
  `BAYAR_PERIKSA` varchar(20) DEFAULT NULL,
  `TOTAL_BAYAR` varchar(20) DEFAULT NULL,
  `ID_RESEP` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_PEMBAYARAN`),
  KEY `ID_RESEP` (`ID_RESEP`),
  CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`ID_RESEP`) REFERENCES `resep` (`ID_RESEP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `periksa`
-- -------------------------------------------
DROP TABLE IF EXISTS `periksa`;
CREATE TABLE IF NOT EXISTS `periksa` (
  `ID_PERIKSA` int(11) NOT NULL AUTO_INCREMENT,
  `DIAGNOSA` text,
  `CATATAN` text,
  `ID_DOKTER` int(11) DEFAULT NULL,
  `ID_PASIEN` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_PERIKSA`),
  KEY `ID_DOKTER` (`ID_DOKTER`),
  KEY `ID_PASIEN` (`ID_PASIEN`),
  CONSTRAINT `periksa_ibfk_1` FOREIGN KEY (`ID_DOKTER`) REFERENCES `dokter` (`ID_DOKTER`),
  CONSTRAINT `periksa_ibfk_2` FOREIGN KEY (`ID_PASIEN`) REFERENCES `pasien` (`ID_PASIEN`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `resep`
-- -------------------------------------------
DROP TABLE IF EXISTS `resep`;
CREATE TABLE IF NOT EXISTS `resep` (
  `ID_RESEP` int(11) NOT NULL AUTO_INCREMENT,
  `TOTAL_HARGA` varchar(20) DEFAULT NULL,
  `ID_OBAT` int(11) DEFAULT NULL,
  `ID_PERIKSA` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_RESEP`),
  KEY `ID_OBAT` (`ID_OBAT`),
  KEY `ID_PERIKSA` (`ID_PERIKSA`),
  CONSTRAINT `resep_ibfk_1` FOREIGN KEY (`ID_OBAT`) REFERENCES `obat` (`ID_OBAT`),
  CONSTRAINT `resep_ibfk_2` FOREIGN KEY (`ID_PERIKSA`) REFERENCES `periksa` (`ID_PERIKSA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `staff`
-- -------------------------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `ID_STAFF` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `NAMA_STAFF` varchar(40) DEFAULT NULL,
  `EMAIL` varchar(40) DEFAULT NULL,
  `USERNAME` varchar(40) DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `NO_TELPN` varchar(12) DEFAULT NULL,
  `ALAMAT` varchar(40) DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(40) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `JOB_DESC` varchar(20) NOT NULL,
  `STATUS` int(5) DEFAULT NULL,
  `pic` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`ID_STAFF`),
  KEY `ID_USER` (`user_id`),
  CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `urut_periksa`
-- -------------------------------------------
DROP TABLE IF EXISTS `urut_periksa`;
CREATE TABLE IF NOT EXISTS `urut_periksa` (
  `ID_URUT` int(11) NOT NULL AUTO_INCREMENT,
  `NO_URUT` int(11) DEFAULT NULL,
  `WAKTU_PERIKSA` time DEFAULT NULL,
  PRIMARY KEY (`ID_URUT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- -------------------------------------------
-- TABLE `user`
-- -------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -------------------------------------------
-- TABLE DATA daftar_periksa
-- -------------------------------------------
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('1','1','1','2017-12-04','Batuk Berdahak','1','03:43:00','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('2','2','1','2017-12-04','Demam tinggi, batuk, pilek','1','05:43:00','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('5','1','1','2017-12-03','Gatal Gatal','1','22:35:00','');
INSERT INTO `daftar_periksa` (`ID_DAFTAR`,`ID_PASIEN`,`ID_DOKTER`,`TANGGAL_PERIKAS`,`KELUHAN`,`STATUS`,`WAKTU_DAFTAR`,`ID_URUT`) VALUES
('6','1','1','2017-12-03','Batuk Berdahak','1','01:06:00','');



-- -------------------------------------------
-- TABLE DATA dokter
-- -------------------------------------------
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('1','1','Wisnu Maulana','Koncopait@gmail.com','wisnu','123','08122971126','Gulunan, Brujul, Jaten, Karanganyar','Islam','Karanganyar','1997-07-16','12480124','Mata','0','10');
INSERT INTO `dokter` (`ID_DOKTER`,`ID_USER`,`NAMA_DOKTER`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`NO_PRAKTEK`,`SPESIALIS`,`STATUS`,`STATUS_AKUN`) VALUES
('2','1','Syahrul Munir','munir@gmail.com','muneron','123','08122971126','Gulunanl Brujul, Jaten, Karanganyar','Islam','Karanganyar','1997-07-16','12124224','Mata','0','10');



-- -------------------------------------------
-- TABLE DATA jadwal_dokter
-- -------------------------------------------
INSERT INTO `jadwal_dokter` (`ID_JADWAL`,`WAKTU_MULAI`,`WAKTU_SELESAI`,`ID_DOKTER`) VALUES
('1','01:00:00','11:30:00','1');
INSERT INTO `jadwal_dokter` (`ID_JADWAL`,`WAKTU_MULAI`,`WAKTU_SELESAI`,`ID_DOKTER`) VALUES
('2','01:00:00','12:00:00','2');



-- -------------------------------------------
-- TABLE DATA migration
-- -------------------------------------------
INSERT INTO `migration` (`version`,`apply_time`) VALUES
('m000000_000000_base','1511447175');
INSERT INTO `migration` (`version`,`apply_time`) VALUES
('m130524_201442_init','1511447178');



-- -------------------------------------------
-- TABLE DATA obat
-- -------------------------------------------
INSERT INTO `obat` (`ID_OBAT`,`NAMA_OBAT`,`HARGA`) VALUES
('1','Paracetamol','10000');
INSERT INTO `obat` (`ID_OBAT`,`NAMA_OBAT`,`HARGA`) VALUES
('2','CTM','3000');



-- -------------------------------------------
-- TABLE DATA pasien
-- -------------------------------------------
INSERT INTO `pasien` (`ID_PASIEN`,`ID_USER`,`NAMA_PASIEN`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`BERAT_BADAN`,`TINGGI_BADAN`,`GOL_DARAH`,`STATUS`) VALUES
('1','1','Wisnu Maulana','koncopait1@gmail.com','Pasien1','pasien','','','','','0000-00-00','0','0','','1');
INSERT INTO `pasien` (`ID_PASIEN`,`ID_USER`,`NAMA_PASIEN`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`BERAT_BADAN`,`TINGGI_BADAN`,`GOL_DARAH`,`STATUS`) VALUES
('2','1','Syahrul munir','koncopait11@gmail.com','Pasien11','pasien','','','','','0000-00-00','0','0','','1');



-- -------------------------------------------
-- TABLE DATA periksa
-- -------------------------------------------
INSERT INTO `periksa` (`ID_PERIKSA`,`DIAGNOSA`,`CATATAN`,`ID_DOKTER`,`ID_PASIEN`) VALUES
('1','Diare','Jangan Makan Pedas','1','2');
INSERT INTO `periksa` (`ID_PERIKSA`,`DIAGNOSA`,`CATATAN`,`ID_DOKTER`,`ID_PASIEN`) VALUES
('3','Pilek','Jangan Begadang','1','2');



-- -------------------------------------------
-- TABLE DATA staff
-- -------------------------------------------
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('2','4','','','','','','','','','','','','','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `staff` (`ID_STAFF`,`user_id`,`NAMA_STAFF`,`EMAIL`,`USERNAME`,`PASSWORD`,`NO_TELPN`,`ALAMAT`,`AGAMA`,`TEMPAT_LAHIR`,`TANGGAL_LAHIR`,`JOB_DESC`,`STATUS`,`pic`,`created_at`,`updated_at`) VALUES
('14','14','Asep Nugroha','','admin','admin','087835121008','','Islam','','','Admin Klinik','','upload/profile/14.png','2017-12-05 03:19:52','2017-12-21 01:25:00');



-- -------------------------------------------
-- TABLE DATA user
-- -------------------------------------------
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('1','WIsnu','42YxZEEV_rP6FFxQ9ViLScpds4H2mdnZ','$2y$13$oSCNwzYVLyOsDHhLunrpN.RsOpP3KirGiV4F5R1dS0gX0SpaV5wOS','','koncopait@gmail.com','10','0','1511945160','2147483647');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('4','admin','zDmrClhhAIisc1ggkIKhgqk2xkZsrWg-','$2y$13$BpOtGXsoaBfZ2Ag4D7/xZOLT9wOG3/YNlKeluC.LaEXXHGLyjLEWe','','admin@gmail.com','10','20','2147483647','2147483647');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('5','alasepnug','_88Vbx3xKgq53oQiZjpXAs0rZbMtFCpQ','$2y$13$Ux5xWmoKfNEACaQxfYLwnu6HrwMA9WHgwym5LLERLGxbOpmeZqFdq','','al.asepnug@gmail.com','10','0','2147483647','2147483647');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('14','andihi','LPaSsvLdNRpGRBigXg6UUSCaKuz_VyBX','$2y$13$kHgdhAHJxTP11W69Y8HR9O3dH757UnO09O3zkqh.ymkMuda265nuC','','andihi@gmail.com','10','20','2147483647','2147483647');
INSERT INTO `user` (`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`role`,`created_at`,`updated_at`) VALUES
('29','nugroho','gwu-xCT1QzY0sWjwB-5hzvU-t3sZeT2b','$2y$13$ib6hcMcgS4Y.LuRhqphpHeMo2Cqr81j9koUh.O3EhAVGNI8cNemt6','','nugroho@gmail.com','10','10','2147483647','2147483647');



-- -------------------------------------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;
-- -------------------------------------------
-- -------------------------------------------
-- END BACKUP
-- -------------------------------------------
