<?php
namespace backend\controllers;

use Yii;

use yii\web\Controller;
use app\models\DaftarPeriksa;
use yii\helpers\Json;


class HighchartsController extends Controller
{
     public function actionIndex()
    {

        $data = Yii::$app->db->createCommand('select 
    ID_DAFTAR,
   sum(jumlah) as jml
   from daftar_periksa')->queryAll();
   return $this->render('grafik', [
   'dgrafik' => $data
   ]);

    
    }


}